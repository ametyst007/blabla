<?php
/**
 * KlasaModel
 * 
 * PHP version 5
 * 
 * @category Model
 * @package  Model
 * @author  Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link wierzba.wzks.uj.edu.pl
 */
namespace Model;
use Silex\Application;

/**
 * Class KlasaModel
 * 
 * @category Model
 * @package  Model
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class KlasaModel
{
    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;
    
    /**
     * Class constructor.
     *
     * @access public
     * @param Application $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }
    
    /**
     * Dodaje klasę do systemu
     * 
     * @param Array $data Associative array
     * 
     * @access public
     * @return void
     */
    public function addKlasa($data)
    {
        $sql = 'INSERT INTO php_Klasa (rocznik, oddzial) VALUES (?,?)';
        $this->_db->executeQuery(
            $sql, array($data['rocznik'],$data['oddzial'])
        );
        
    }
    
     /**
     * Wyświetla dane dotyczące wszystkich klas
     * 
     * @access public
     * @return Array 
     */
    public function getAll()
    {
        $sql = 'SELECT * FROM php_Klasa ORDER BY rocznik, oddzial;';
        return $this->_db->fetchAll($sql);  
    }
    
    /**
     * Edytuje dane klasy
     * 
     * @param Array $data Associative array contains information about class
     * 
     * @access public
     * @return void
     */
    public function editKlasa($data)
    {

        if (isset($data['id']) && ctype_digit((string) $data['id'])) {
            $sql = 'UPDATE php_Klasa SET rocznik = ?, oddzial = ? '
                    . 'WHERE idKlasa = ?;';
            $this->_db->executeQuery(
                $sql, array($data['rocznik'], $data['oddzial'], $data['id'])
            );
            
        } else {
            $sql = 'INSERT INTO php_Klasa (rocznik, oddzial) VALUES (?,?);';
            $this->_db->executeQuery(
                $sql, array($data['rocznik'], $data['oddzial'])
            );
        }
    }
    
    /**
     * Gets one class
     * 
     * @param Integer $id id klasy
     * 
     * @access public
     * @return Array Associative array contains information about class
     */
    public function getKlasa($id)
    {
        if (($id != '') && ctype_digit((string) $id)) {
            $sql = 'SELECT * FROM php_Klasa WHERE idKlasa = ? LIMIT 1;';
            return $this->_db->fetchAssoc($sql, array((int) $id));
        } else {
            return array();
        }
    }
    
    /**
     * Sprawdza, czy istnieje klasa o takim id
     * 
     * @param Integer $idKlasa id klasy from request
     * 
     * @access public
     * @return bool True if exists
     */
    public function sprawdzKlase($idKlasa)
    {
        $sql = 'SELECT * FROM php_Klasa WHERE idKlasa=?';
        $result = $this->_db->fetchAll($sql, array($idKlasa));

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Usuwa klasę
     * 
     * @param Array $data Associative array contains information about class
     * 
     * @access public
     * @return void
     */
    public function usunKlase($data)
    {
        $sql = 'DELETE FROM `php_Klasa` WHERE `idKlasa`= ?';
        $this->_db->executeQuery($sql, array($data['id']));
    }
    
    /**
     * Znajduje klasę o podanych parametrach
     * 
     * @param Integer $rocznik
     * @param Char $oddzial
     * 
     * @access public
     * @return Array Associative array contains information about class
     */
    public function getKlasabyData($rocznik, $oddzial)
    {
        $sql = 'SELECT * FROM php_Klasa WHERE rocznik = ? AND oddzial = ?';
        return $this->_db->fetchAssoc($sql, array($rocznik, $oddzial));
    }
    
    /**
     * Tworzy tablicę klas do użycia w liście rozwijanej
     * 
     * @param Array $klasy
     * 
     * @access public
     * @return Array with classes to select
     */
    public function klasaToSelect($klasy)
    {
        $klasyfinal = array();
        foreach ($klasy as $arr) {
            $klasyfinal[$arr['idKlasa']] = $arr['rocznik'].$arr['oddzial'];
        }
        return $klasyfinal;
    }
    
    /**
     * Dodaje informację o tym, jakich przedmiotów uczy się klasa
     * 
     * @param Array $data Associative array contains information about class
     * @param Integer $idp id przedmiotu
     * 
     * @access public
     * @return void
     */
    public function przedmiotyKlasa($data, $idp)
    {

        $przedmioty = array();
        $przedmioty = $data['przedmiot'];
        //var_dump($przedmioty);die();
        foreach ($przedmioty as $przedmiot) {
            $sql = "INSERT INTO php_Klasa_Przedmiot (idPrzedmiot, idKlasa) "
                    . "VALUES (?,?)";
            $this->_db->executeQuery($sql, array($przedmiot, $idp));
        }
    }
}
