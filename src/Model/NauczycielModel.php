<?php
/**
 * NauczycielModel
 * 
 * PHP version 5
 * 
 * @category Model
 * @package  Model
 * @author  Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link wierzba.wzks.uj.edu.pl
 */
namespace Model;

use Silex\Application;
/**
 * Class NauczycielModel
 * 
 * @category Model
 * @package  Model
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class NauczycielModel
{
    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;
    
     /**
     * Class constructor.
     *
     * @access public
     * @param Application $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }
    
    /**
     * Dodaje dane jednego nauczyciela
     * 
     * @param Array $data Associative array
     * 
     * @access public
     * @return void
     */
    public function addNauczyciel($data)
    {
        $sql = "INSERT INTO `php_Nauczyciel` (iduser, `tytul`, `idKlasa`)"
                . " VALUES (?,?,?);";
        $this->_db->executeQuery(
            $sql, array($data['id'], $data['tytul'], $data['idKlasa'])
        );
    }
    /**
     * Dodaje przedmioty, których uczy nauczyciel
     * 
     * @param  Array $data Associative array
     * 
     * @access public
     * @return void
     */
    public function przedmiotyNauczyciel($data)
    {

        $przedmioty = array();
        $przedmioty = $data['przedmiot'];
        //var_dump($przedmioty);die();
        foreach ($przedmioty as $przedmiot) {
            $sql = "INSERT INTO php_Nauczyciel_Przedmiot (idPrzedmiot, iduser)"
                    . " VALUES (?,?)";
            $this->_db->executeQuery(
                $sql, array($przedmiot, $data['id'])
            );
        }
    }

    /**
     * Wyświetla dane dotyczące nauczycieli
     * 
     * @access public
     * @return Array Associative teachers array
     */
    public function daneNauczyciel()
    {
        $sql = "SELECT php_Nauczyciel.iduser, imie, nazwisko, login,
                tytul, rocznik, oddzial FROM php_users 
                JOIN php_Nauczyciel ON php_users.iduser=php_Nauczyciel.iduser
                JOIN users_roles ON php_users.iduser=users_roles.iduser
                LEFT JOIN php_Klasa ON php_Nauczyciel.idKlasa=php_Klasa.idKlasa
                WHERE users_roles.idroles=2 OR users_roles.idroles=3";

        return $this->_db->fetchAll($sql);
    }
    
    /**
     * Wyświetla pełne dane konkretnego nauczyciela
     * 
     * @param Integer $id id użytkownika
     * 
     * @access public
     * @return Array Associative array contains information about teacher
     */
    public function danejedenNauczyciel($id)
    {
        $sql = "SELECT imie, nazwisko, login, tytul, idKlasa AS wychowawca
                FROM php_users JOIN php_Nauczyciel
                ON php_users.iduser=php_Nauczyciel.iduser
                WHERE php_Nauczyciel.iduser=?";
        return $this->_db->fetchAssoc($sql, array((int) $id));
    }
    
    /**
     * Gets one teacher
     * 
     * @param Integer $id id nauczyciela
     * 
     * @access public
     * @return Array Associative array contains information about teacher
     */
    public function getNauczyciel($id)
    {
        if (($id != '') && ctype_digit((string) $id)) {
            $sql = 'SELECT * FROM php_Nauczyciel WHERE iduser = ? LIMIT 1;';
            return $this->_db->fetchAssoc($sql, array((int) $id));
        } else {
            return array();
        }
    }
    
    /**
     * Edytuje dodatkowe dane nauczyciela
     * 
     * @param Array $data Associative array contains information about teacher
     * 
     * @access public
     * @return void
     */
    public function editNauczyciel($data)
    {

        if (isset($data['id']) && ctype_digit((string) $data['id'])) {
            $sql = 'UPDATE php_Nauczyciel SET tytul = ?, idKlasa = ? '
                    . 'WHERE iduser = ?';
            $this->_db->executeQuery(
                $sql, array($data['tytul'], $data['idKlasa'], $data['id'])
            );
        
            
        } else {
            $sql = 'INSERT INTO php_Nauczyciel (tytul, idKlasa) VALUES (?,?);';
            $this->_db->executeQuery(
                $sql, array($data['tytul'], $data['idKlasa'])
            );
        }
    }
    
    /**
     * Usuwa dodatkowe dane nauczyciela
     * 
     * @param Array $data Associative array contains information about teacher
     * 
     * @access public
     * @return void
     */
    public function usunDaneNauczyciel($data)
    {
        $sqlj = 'DELETE FROM users_roles WHERE iduser = ?';
        $this->_db->executeQuery($sqlj, array($data['iduser']));
        $sql = 'DELETE FROM `php_Nauczyciel` WHERE `iduser`= ?';
        $this->_db->executeQuery($sql, array($data['iduser']));
    }
    
    /**
     * Sprawdza, czy istnieje nauczyciel o takim id
     * 
     * @param Integer $iduser id nauczyciela from request
     * 
     * @access public
     * @return bool True if exists
     */
    public function sprawdzNauczyciel($iduser)
    {
        $sql = 'SELECT * FROM php_Nauczyciel WHERE iduser=?';
        $result = $this->_db->fetchAll($sql, array($iduser));

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Sprawdza, kto jest wychowawcą danej klasy
     * 
     * @param Integer $idKlasa id klasy
     * 
     * @access public
     * @return Array Associative array zawiera informacje na temat wychowawcy
     */
    public function getwychowawca($idKlasa)
    {
        $sql = 'SELECT * FROM php_Nauczyciel WHERE idKlasa = ?';
        return $this->_db->fetchAssoc($sql, array($idKlasa));
    }
    
    /**
     * Sprawdza id ostatnio dodanego użytkownika
     * 
     * @access public
     * @return Array Associative array contains information last user
     */
    public function lastID()
    {
        $sql = 'SELECT iduser FROM php_users ORDER BY iduser DESC LIMIT 1';
        return $this->_db->fetchAssoc($sql);
    }

}
