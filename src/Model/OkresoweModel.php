<?php
/**
 * OkresoweModel
 * 
 * PHP version 5
 * 
 * @category Model
 * @package  Model
 * @author  Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link wierzba.wzks.uj.edu.pl
 */
namespace Model;

use Silex\Application;

/**
 * Class OkresoweModel
 * 
 * @category Model
 * @package  Model
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class OkresoweModel
{
    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;
    
    /**
     * Class constructor.
     *
     * @access public
     * @param Application $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }
    
    /**
     * Dodaje oceny okresowe
     * 
     * @param Array $data Associative array zawiera informacje
     * o ocenach okresowych
     * 
     * @access public
     * @return void
     */
    public function Okresowe($data)
    {
        $sql = 'INSERT INTO php_Uczen_Przedmiot (idPrzedmiot, iduser,'
                . ' ocena_semestr, ocena_rok) VALUES(?,?,?,?)';
        $this->_db->executeQuery(
            $sql, array($data['idPrzedmiot'], $data['iduser'],
                    $data['osemestr'], $data['orok'])
        );
    }
    
     /**
     * Edytuje oceny okresowe
     * 
     * @param Array $data Associative array zawiera informacje
     * o ocenach okresowych
     * 
     * @access public
     * @return void
     */
    public function Okresoweedit($data)
    {
        $sql = 'UPDATE php_Uczen_Przedmiot SET idPrzedmiot=?, iduser=?,'
                . ' ocena_semestr=?, ocena_rok=? WHERE id=?';
        $this->_db->executeQuery(
            $sql, array($data['idPrzedmiot'], $data['iduser'],
                    $data['osemestr'], $data['orok'], $data['id'])
        );
    }
    
    /**
     * Pobiera rekord o danym id z tabeli z ocenami okresowymi
     * 
     * @param Integer $id id
     * 
     * @access public
     * @return Array
     */
    public function getokres($id)
    {
        if (($id != '') && ctype_digit((string) $id)) {

            $sql = 'SELECT * FROM php_Uczen_Przedmiot WHERE id = ?';
            return $this->_db->fetchAssoc($sql, array((int) $id));
        } else {
            return array();
        }
    }
    
     /**
     * Sprawdza, czy istnieje rekord z ocenami okresowymi o takim id
     * 
     * @param Integer $id id ocen okresowych from request
     * 
     * @access public
     * @return bool True if exists
     */
    public function sprawdzokres($id)
    {
        $sql = 'SELECT * FROM php_Uczen_Przedmiot WHERE id=?';
        $result = $this->_db->fetchAll($sql, array($id));

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
     /**
     * Wyświetla wszystkie oceny okresowe
     * 
     * @access public
     * @return Array 
     */
    public function getOkresowe()
    {
        $sql = 'SELECT nazwa, id, iduser, ocena_semestr, ocena_rok
              FROM php_Uczen_Przedmiot JOIN php_Przedmiot
              ON php_Uczen_Przedmiot.idPrzedmiot=php_Przedmiot.idPrzedmiot';
        return $this->_db->fetchAll($sql);
    }
      /**
     * Wyświetla wszystkie oceny okresowe danego ucznia
     * @param Integer $id id ucznia
      * 
     * @access public
     * @return Array 
     */
    public function getOkresoweuczen($id)
    {
        $sql = 'SELECT nazwa, id, iduser, ocena_semestr, ocena_rok
            FROM php_Uczen_Przedmiot JOIN php_Przedmiot 
            ON php_Uczen_Przedmiot.idPrzedmiot=php_Przedmiot.idPrzedmiot
            WHERE iduser =?';
        return $this->_db->fetchAll($sql, array($id));
    }
    /**
     * Oceny danego ucznia z konkretnego przedmiotu
     * 
     * @param Integer $iduser
     * @param Integer $idprzedmiot
     * 
     * @access public
     * @return Array
     */
    public function getOcenaById($iduser, $idprzedmiot)
    {
        $sql = 'SELECT * FROM php_Uczen_Przedmiot WHERE iduser = ?'
                . ' AND idPrzedmiot = ?';
        return $this->_db->fetchAssoc($sql, array($iduser, $idprzedmiot));
    }
}
