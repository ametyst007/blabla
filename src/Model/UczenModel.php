<?php
/**
 * UczenModel
 * 
 * PHP version 5
 * 
 * @category Model
 * @package  Model
 * @author  Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link wierzba.wzks.uj.edu.pl
 */
namespace Model;

use Silex\Application;

/**
 * Class UczenModel
 * 
 * @category Model
 * @package  Model
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class UczenModel
{
    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;
    
    /**
     * Class constructor.
     *
     * @access public
     * @param Application $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }
    
    /**
     * Dodaje dane ucznia do systemu
     * 
     * @param Array $data Associative array
     * 
     * @access public
     * @return void
     */
    public function addUczen($data)
    {
        $sql = "INSERT INTO `php_Uczen` (`iduser`,`data_urodzenia`,"
                . " `miejscowosc`, `ulica`, `nr_domu`, `kod_pocztowy`,"
                . " `kontakt`, `idKlasa`) VALUES (?,?,?,?,?,?,?,?);";
        $this->_db->executeQuery(
            $sql, array($data['id'], $data['data'], $data['miejscowosc'],
            $data['ulica'], $data['nr_domu'], $data['kod_pocztowy'],
            $data['kontakt'], $data['klasa'])
        );
    }
    
     /**
     * Wyświetla dane uczniów
     * 
     * @access public
     * @return Array 
     */
    public function daneUcznia()
    {
        $sql = "SELECT imie, nazwisko, login, data_urodzenia, idKlasa
            FROM php_users JOIN php_Uczen ON php_users.iduser=php_Uczen.iduser
                JOIN users_roles ON php_users.iduser=users_roles.iduser
                WHERE users_roles.idroles=4";
        return $this->_db->fetchAll($sql);
    }
    
     /**
     * Gets one student
     * 
     * @param Integer $id id przedmiotu
     * 
     * @access public
     * @return Array Associative array contains information about student
     */
    public function getUczen($id)
    {
        if (($id != '') && ctype_digit((string) $id)) {
            $sql = 'SELECT * FROM php_Uczen WHERE iduser = ? LIMIT 1;';
            return $this->_db->fetchAssoc($sql, array((int) $id));
        } else {
            return array();
        }
    }
    
     /**
     * Edytuje dane ucznia
     * 
     * @param Array $data Associative array contains information about student
     * 
     * @access public
     * @return void
     */
    public function editUczen($data)
    {

        if (isset($data['id']) && ctype_digit((string) $data['id'])) {
            $sql = 'UPDATE php_Uczen SET data_urodzenia = ?, miejscowosc = ?,'
                    . ' ulica = ?, nr_domu = ?, kod_pocztowy = ?, kontakt = ?,'
                    . ' idKlasa = ?  WHERE iduser = ?;';
            $this->_db->executeQuery(
                $sql, array($data['data'], $data['miejscowosc'],
                        $data['ulica'], $data['nr_domu'], $data['kod_pocztowy'],
                        $data['kontakt'], $data['klasa'], $data['id'])
            );
        } else {
            $sql = 'INSERT INTO php_Uczen (data_urodzenia, miejscowosc, ulica,'
                    . ' nr_domu, kod_pocztowy, kontakt) VALUES (?,?,?,?,?,?);';
            $this->_db->executeQuery(
                $sql, array($data['data'], $data['miejscowosc'],
                        $data['ulica'], $data['nr_domu'], $data['kod_pocztowy'],
                        $data['kontakt'])
            );
        }
    }
    
    /**
     * Usuwa dane ucznia
     * 
     * @param Array $data Associative array contains information about student
     * 
     * @access public
     * @return void
     */
    public function usunDaneUczen($data)
    {
        $sqlj = 'DELETE FROM users_roles WHERE iduser = ?';
        $this->_db->executeQuery($sqlj, array($data['iduser']));
        $sql = 'DELETE FROM `php_Uczen` WHERE `iduser`= ?';
        $this->_db->executeQuery($sql, array($data['iduser']));
    }

    /**
     * Sprawdza, czy istnieje przedmiot o takim id
     * 
     * @param Integer $iduser id ucznia from request
     * 
     * @access public
     * @return bool True if exists
     */
    public function sprawdzUczen($iduser)
    {
        $sql = 'SELECT * FROM php_Uczen WHERE iduser=?';
        $result = $this->_db->fetchAll($sql, array($iduser));

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Wyświetla oceny danego ucznia
     * 
     * @param Integer $iduser
     * 
     * @access public
     * @return Array
     */
    public function ocenyUcznia($iduser)
    {
        $sql = 'SELECT idOcena, stopien, data_wystawienia, nazwa, komentarz
            FROM php_Ocena JOIN php_Przedmiot
            ON php_Ocena.idPrzedmiot=php_Przedmiot.idPrzedmiot
                WHERE iduser=? ORDER BY nazwa';


        return $this->_db->fetchAll($sql, array($iduser));
    }
    
    /**
     * Sprawdza id ostatnio dodanego ucznia
     * 
     * @access public
     * @return Array
     */
    public function lastID()
    {
        $sql = 'SELECT iduser FROM php_users ORDER BY iduser DESC LIMIT 1';
        return $this->_db->fetchAssoc($sql);
    }
    
    /**
     * Wyświetla listę uczniów danej klasy
     * 
     * @param Integer $idKlasa
     * 
     * @access public
     * @return Array
     */
    public function uczniowieDanejKlasy($idKlasa)
    {
        $sql = 'SELECT imie, nazwisko FROM php_users JOIN php_Uczen 
                ON php_users.iduser=php_Uczen.iduser
                JOIN users_roles ON php_users.iduser=users_roles.iduser
                WHERE idKlasa = ? AND idroles=4';
        return $this->_db->fetchAll($sql, array($idKlasa));
    }
    
    /**
     * Lista uczniów zapisanych na dany przedmiot
     * 
     * @param Integer $idPrzedmiot
     * 
     * @access public
     * @return Array
     */
    public function uczniowieDanyPrzedmiot($idPrzedmiot)
    {
        $sql = 'SELECT imie, nazwisko FROM php_users JOIN php_Uczen 
                ON php_users.iduser=php_Uczen.iduser
                JOIN users_roles ON php_users.iduser=users_roles.iduser
                LEFT JOIN php_Klasa_Przedmiot
                ON php_Uczen.idKlasa=php_Klasa_Przedmiot.idKlasa 
                WHERE idPrzedmiot = ? AND idroles=4';
        return $this->_db->fetchAll($sql, array($idPrzedmiot));
    }
}
