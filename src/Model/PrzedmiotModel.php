<?php
/**
 * PrzedmiotModel
 * 
 * PHP version 5
 * 
 * @category Model
 * @package  Model
 * @author  Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link wierzba.wzks.uj.edu.pl
 */
namespace Model;

use Silex\Application;

/**
 * Class PrzedmiotModel
 * 
 * @category Model
 * @package  Model
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class PrzedmiotModel
{
    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;
    
    /**
     * Class constructor.
     *
     * @access public
     * @param Application $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }
    
    /**
     * Dodaje przedmiot do systemu
     * 
     * @param Array $data Associative array
     * 
     * @access public
     * @return void
     */
    public function addPrzedmiot($data)
    {
        $sql = 'INSERT INTO php_Przedmiot (nazwa) VALUES (?)';
        $this->_db->executeQuery($sql, array($data['nazwa']));
    }
    
     /**
     * Wyświetla dane dotyczące wszystkich przedmiotów
     * 
     * @access public
     * @return Array 
     */
    public function getAll()
    {
        $sql = 'SELECT * FROM php_Przedmiot ORDER BY nazwa;';
        return $this->_db->fetchAll($sql);
    }
    
    /**
     * Tworzy tablicę przedmiotów do użycia w liście rozwijanej
     * 
     * @param Array $przedmioty
     * 
     * @access public
     * @return Array with subjects to select
     */
    public function przedmiotToSelect($przedmioty)
    {
        $przedmiotyfinal = array();
        foreach ($przedmioty as $arr) {
            $przedmiotyfinal[$arr['idPrzedmiot']] = $arr['nazwa'];
        }
        return $przedmiotyfinal;
    }
    
    /**
     * Gets one subject
     * 
     * @param Integer $id id przedmiotu
     * 
     * @access public
     * @return Array Associative array contains information about subject
     */
    public function getPrzedmiot($id)
    {
        $sql = 'SELECT * FROM php_Przedmiot WHERE idPrzedmiot = ? LIMIT 1';

        return $this->_db->fetchAssoc($sql, array($id));
    }
    
     /**
     * Sprawdza, czy istnieje przedmiot o takim id
     * 
     * @param Integer $idPrzedmiot id przedmiotu from request
     * 
     * @access public
     * @return bool True if exists
     */
    public function sprawdzPrzedmiot($idPrzedmiot)
    {
        $sql = 'SELECT * FROM php_Przedmiot WHERE idPrzedmiot=?';
        $result = $this->_db->fetchAll($sql, array($idPrzedmiot));

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Edytuje dane przedmiotu
     * 
     * @param Array $data Associative array contains information about subject
     * 
     * @access public
     * @return void
     */
    public function editPrzedmiot($data)
    {

        if (isset($data['id']) && ctype_digit((string) $data['id'])) {
            $sql = 'UPDATE php_Przedmiot SET nazwa = ? WHERE idPrzedmiot = ?;';
            $this->_db->executeQuery($sql, array($data['nazwa'], $data['id']));
        } else {
            $sql = 'INSERT INTO php_Przedmiot (nazwa) VALUES (?);';
            $this->_db->executeQuery($sql, array($data['nazwa']));
        }
    }
    
    /**
     * Usuwa przedmiot
     * 
     * @param Array $data Associative array contains information about subject
     * 
     * @access public
     * @return void
     */
    public function usunPrzedmiot($data)
    {
        $sql = 'DELETE FROM `php_Przedmiot` WHERE `idPrzedmiot`= ?';
        $this->_db->executeQuery($sql, array($data['idPrzedmiot']));
    }
    
    /**
     * Szuka przedmiotu po nazwie
     * @param String $nazwa
     * 
     * @access public
     * @return Array
     */
    public function getPrzedmiotbyNazwa($nazwa)
    {
        $sql = 'SELECT * FROM php_Przedmiot WHERE nazwa = ?';
        return $this->_db->fetchAssoc($sql, array($nazwa));
    }
    /**
     * Sprawdza czy uczeń należy do klasy zapisanej na dany przedmiot
     * @param Array $data
     * 
     * @access public
     * @return bool True if exists
     */
   public function klasaPrzedmiot($data)
   {
       $sql = 'SELECT * FROM php_Klasa_Przedmiot
               JOIN php_Uczen ON php_Klasa_Przedmiot.idKlasa=php_Uczen.idKlasa
               WHERE iduser=? AND idPrzedmiot=?';
        $result = $this->_db->fetchAll(
            $sql, array($data['iduser'], $data['idPrzedmiot'])
        );

        if ($result) {
            return true;
        } else {
            return false;
        }
  
   }
   /**
    * Sprawdza, czy nauczyciel wykłada ten przedmiot
    * @param Array $data tablica danych z formularza
    * @param Integer $id id aktualnie zalogowanego nauczyciela
    * 
    * @access public
    * @return bool Tru if exists
    */
   public function nauczycielPrzedmiot($data, $id)
   {
       $sql = 'SELECT * FROM php_Nauczyciel_Przedmiot
               WHERE iduser=? AND idPrzedmiot=?';
       
       $result = $this->_db->fetchAll(
            $sql, array($id, $data['idPrzedmiot']) //idnaucz
        );

        if ($result) {
            return true;
        } else {
            return false;
        }
  
   }
       
               
   
}
