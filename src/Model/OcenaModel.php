<?php
/**
 * OcenaModel
 * 
 * PHP version 5
 * 
 * @category Model
 * @package  Model
 * @author  Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link wierzba.wzks.uj.edu.pl
 */
namespace Model;

use Silex\Application;

/**
 * Class OcenaModel
 * 
 * @category Model
 * @package  Model
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class OcenaModel
{
    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;
    
    /**
     * Class constructor.
     *
     * @access public
     * @param Application $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }
    
     /**
     * Dodaje ocenę do systemu
     * 
     * @param Array $data Associative array
     * 
     * @access public
     * @return void
     */
    public function addOcena($data)
    {
        $sql = 'INSERT INTO php_Ocena (stopien, data_wystawienia, idPrzedmiot,'
                . ' iduser, komentarz) VALUES (?, ?, ?, ?, ?)';
        $this->_db->executeQuery(
            $sql, array($data['stopien'], $data['data'],
                    $data['idPrzedmiot'], $data['iduser'], $data['komentarz'])
        );
    }
    
     
     /**
     * Wyświetla wszystkie oceny
     * 
     * @access public
     * @return Array 
     */
    public function getAll()
    {
        $sql = 'SELECT idOcena, stopien, data_wystawienia, php_Ocena.iduser,
                komentarz, nazwa, imie, nazwisko FROM php_Ocena
                JOIN php_Przedmiot
                ON php_Ocena.idPrzedmiot=php_Przedmiot.idPrzedmiot
                JOIN php_users ON php_Ocena.iduser=php_users.iduser';
        return $this->_db->fetchAll($sql);
    }
    
     /**
     * Wybiera oceny wybranego ucznia z wybranego przedmiotu
     * 
     * @access public
     * @return Array 
     */
    public function getWybrane($data)
    {

        $sql = 'SELECT idOcena, stopien, data_wystawienia, komentarz, nazwa,
               imie, nazwisko, php_Ocena.iduser FROM php_Ocena
               JOIN php_Przedmiot
               ON php_Ocena.idPrzedmiot=php_Przedmiot.idPrzedmiot
               LEFT JOIN php_Uczen ON php_Ocena.iduser=php_Uczen.iduser
               JOIN php_users ON php_Ocena.iduser=php_users.iduser
               WHERE php_Ocena.iduser =? AND php_Ocena.idPrzedmiot =?';
        return $this->_db->fetchAll(
            $sql, array($data['iduczen'], $data['idPrzedmiot'])
        );
    }
    
     /**
     * Wszystkie oceny z wybranego przedmiotu
     * 
     * @access public
     * @return Array 
     */
    public function getWybranePrzedmiot($data)
    {
        $sql = 'SELECT idOcena, stopien, data_wystawienia, komentarz, nazwa,
               imie, nazwisko, php_Ocena.iduser FROM php_Ocena
               JOIN php_Przedmiot 
               ON php_Ocena.idPrzedmiot=php_Przedmiot.idPrzedmiot
               LEFT JOIN php_Uczen ON php_Ocena.iduser=php_Uczen.iduser
               JOIN php_users ON php_Ocena.iduser=php_users.iduser
               WHERE php_Ocena.idPrzedmiot =?';
        return $this->_db->fetchAll($sql, array($data['idPrzedmiot']));
    }
    
     /**
     * Wszystkie oceny wybranego ucznia
     * 
     * @access public
     * @return Array 
     */
    public function getWybraneUczen($data)
    {
        $sql = 'SELECT idOcena, stopien, data_wystawienia, komentarz, nazwa,
               imie, nazwisko, php_Ocena.iduser FROM php_Ocena
               JOIN php_Przedmiot 
               ON php_Ocena.idPrzedmiot=php_Przedmiot.idPrzedmiot
               LEFT JOIN php_Uczen ON php_Ocena.iduser=php_Uczen.iduser
               JOIN php_users ON php_Ocena.iduser=php_users.iduser
               WHERE php_Ocena.iduser =?';
        return $this->_db->fetchAll($sql, array($data['iduczen']));
    }
    
    /**
     * Zapis oceny po edycji
     * @param Array $data
     * @access public
     * @return void
     */
    public function saveOcena($data)
    {
        if (isset($data['id']) && ctype_digit((string) $data['id'])) {

            $sql = 'UPDATE php_Ocena SET stopien = ?, data_wystawienia = ?,'
                    . ' idPrzedmiot = ?, iduser = ?, komentarz = ? '
                    . 'WHERE idOcena = ?';
            $this->_db->executeQuery(
                $sql, array($data['stopien'], $data['data'],
                        $data['idPrzedmiot'], $data['iduser'],
                        $data['komentarz'], $data['id'])
            );
        } else {
            $sql = 'INSERT INTO php_Ocena (stopien, data_wystawienia,'
                    . ' idPrzedmiot, iduser, komentarz) VALUES (?,?,?,?,?)';
            $this->_db->executeQuery(
                $sql, array($data['stopien'], $data['data'],
                        $data['idPrzedmiot'], $data['iduser'],
                        $data['komentarz'])
            );
        }
    }
    
     /**
     * Sprawdza, czy istnieje ocena o takim id
     * 
     * @param Integer $idOcena id oceny from request
     * 
     * @access public
     * @return bool True if exists
     */
    public function sprawdzOcena($idOcena)
    {
        $sql = 'SELECT * FROM php_Ocena WHERE idOcena=?';
        $result = $this->_db->fetchAll(
            $sql, array($idOcena)
        );

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Gets one grade
     * 
     * @param Integer $id id oceny
     * 
     * @access public
     * @return Array Associative array contains information about grades
     */
    public function getOcena($id)
    {
        if (($id != '') && ctype_digit((string) $id)) {

            $sql = 'SELECT idOcena, stopien, data_wystawienia, idPrzedmiot,'
                    . ' iduser, komentarz FROM php_Ocena WHERE idOcena = ?';
            return $this->_db->fetchAssoc(
                $sql, array((int) $id)
            );
            
        } else {
            return array();
        }
    }
    
    /**
     * Usuwa ocenę
     * 
     * @param Array $data Associative array contains information about class
     * 
     * @access public
     * @return void
     */
    public function usunOcene($data)
    {
        $sql = 'DELETE FROM `php_Ocena` WHERE `idOcena`= ?';
        $this->_db->executeQuery($sql, array($data['idOcena']));
    }
    
    /**
     * Wylicza średnią ocen cząstkowych jednego ucznia
     * z poszczególnych przedmiotów
     * 
     * @param Integer $id id ucznia
     * 
     * @access public
     * @return Array
     */
    public function avgOcena($id)
    {
        $sql = 'SELECT imie, nazwisko, nazwa, AVG(stopien) AS srednia 
                FROM php_Ocena
                JOIN php_users ON php_Ocena.iduser=php_users.iduser
                JOIN php_Przedmiot 
                ON php_Ocena.idPrzedmiot=php_Przedmiot.idPrzedmiot
                WHERE php_users.iduser=? GROUP BY nazwa';
        return $this->_db->fetchAll($sql, array($id));
    }
    
    /**
     * Wylicza średnią uczniów z poszczególnych przedmiotów
     * 
     * @param Integer $idPrzedmiot id przedmiotu
     * 
     * @access public
     * @return Array
     */
    public function avgOcenaAll($idPrzedmiot)
    {
        $sql = 'SELECT imie, nazwisko, nazwa, AVG(stopien) AS srednia 
                FROM php_Ocena
                JOIN php_users ON php_Ocena.iduser=php_users.iduser
                JOIN php_Przedmiot ON 
                php_Ocena.idPrzedmiot=php_Przedmiot.idPrzedmiot
                WHERE php_Ocena.idPrzedmiot=? GROUP BY nazwa, 
                php_Ocena.iduser';
        return $this->_db->fetchAll($sql, array($idPrzedmiot));
    }

}
