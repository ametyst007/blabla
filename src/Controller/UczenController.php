<?php
/**
 * Uczen Controller
 * 
 * PHP version 5
 * 
 * @category Controller
 * @package  Controller
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 */
namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Model\UczenModel;
use Model\KlasaModel;
use Model\PrzedmiotModel;

/**
 * Class UczenController
 * 
 * @category Controller
 * @package  Controller
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Silex\Application
 * @uses Silex\ControllerProviderInterface
 * @uses Symfony\Component\HttpFoundation\Request
 * @uses Symfony\Component\Validator\Constraints
 * @uses Model\UczenModel 
 * @uses Model\KlasaModel
 * @uses Model\PrzedmiotModel
 */
class UczenController implements ControllerProviderInterface
{
    /**
     * UczenModel object
     * 
     * @var $_model
     * @access protected
     */
    protected $_model;
    
    /**
     * Connection
     *
     * @param Application $app application object
     *
     * @access public
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->_model = new UczenModel($app);
        $this->_klasa = new KlasaModel($app);
        $this->_przedmiot = new PrzedmiotModel($app);
        $uczenController = $app['controllers_factory'];
        $uczenController->get('/', array($this, 'index'))->bind('/uczen/');
        $uczenController->get('/lista/{id}', array($this, 'lista'))
                ->bind('/uczen/lista');
        $uczenController->get(
            '/uczen_przedmiot/{id}', array(
                    $this, 'uczen_przedmiot')
        )->bind(
            '/uczen/uczen_przedmiot'
        );
        $uczenController->match('/add/', array($this, 'add'))
                ->bind('/uczen/add');
        $uczenController->match('/edit/{id}', array($this, 'edit'))
                ->bind('/uczen/edit');
        $uczenController->match('/delete/{id}', array($this, 'delete'))
                ->bind('/uczen/delete');
        $uczenController->get('/view/', array($this, 'view'))
                ->bind('/uczen/view');
        return $uczenController;
    }
    
    /**
     * Wyświetl listę uczniów 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function index(Application $app)
    {
        $uczenModel = new UczenModel($app);
        $uczniowie = $uczenModel->daneUcznia();
        return $app['twig']->render(
            'uczen/index.twig', array(
                'uczniowie' => $uczniowie)
        );
    }
    
    /**
     * Wyświetl listę uczniów danej klasy 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function lista (Application $app, Request $request )
    {
        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_klasa->sprawdzKlase($id);
        if ($sprawdz) {
           // $klasa = $this->_klasa->getKlasa($id);
        $uczenModel = new UczenModel($app);
        $uczniowie = $uczenModel->uczniowieDanejKlasy($id);
        return $app['twig']->render(
            'uczen/lista.twig', array(
                'uczniowie' => $uczniowie)
        );
        
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                    'type' => 'danger',
                    'content' => 'Klasa o tym id nie istnieje'
                    )
            );
        }
    }

     /**
     * Wyświetl listę uczniów zapisanych na przedmiot 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function uczen_przedmiot(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_przedmiot->sprawdzPrzedmiot($id);
        if ($sprawdz) {
            $uczenModel = new UczenModel($app);
            $uczniowie = $uczenModel->uczniowieDanyPrzedmiot($id);
            
        return $app['twig']->render(
            'uczen/przedmioty.twig', array(
                'uczniowie' => $uczniowie)
        );
        
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                    'type' => 'danger',
                    'content' => 'Przedmiot o tym id nie istnieje'
                    )
            );
        }
        
    }
    
    /**
     * Dodaj informacje na temat nowego ucznia
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function add(Application $app, Request $request)
    {
        $klasaModel = new KlasaModel($app);
        $klasy = $klasaModel->getAll();
        $klasyToSelect = $klasaModel->klasaToSelect($klasy);

        $idarray = $this->_model->lastID();
        $id = (int) $idarray['iduser'];
        //var_dump($id);die();
        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add(
                    'id', 'hidden', array(
                        'data' => $id,)
                )
                ->add(
                    'data', 'date', array(
                    'input' => 'string',
                    'widget' => 'choice',
                    'years' => range(1990, 2014),
                    'format' => 'yyyy-MM-dd',
                    'label' => 'data urodzenia',
                    'empty_value' => '',
                    'invalid_message' => 'Wprowadzona data jest niepoprawna!'
                )
                )
                ->add(
                    'miejscowosc', 'text', array(
                    'label' => 'miejscowość',
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Length(
                            array(
                            'min' => 2,
                            'max' => 30,)
                        ),
                        new Assert\Regex(
                            array(
                            'pattern' => '/^[A-Z][a-zżźćńółęąś\s]+$/',
                            'message' => 'Niepoprawna nazwa'
                                )
                        )
                        )
                )
                )
                ->add(
                    'ulica', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Length(
                            array(
                            'min' => 2,
                            'max' => 50,)
                        ),
                        new Assert\Regex(
                            array(
                            'pattern' => '/^[A-Z][a-zżźćńółęąś\s]+$/',
                            'message' => 'Niepoprawna nazwa'
                                )
                        )
                        )
                )
                )
                ->add(
                    'nr_domu', 'number', array(
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Regex(
                            array(
                            'pattern' => '/^\d+$/',
                            'message' => 'nr domu musi być liczbą',
                                )
                        )
                        )
                )
                )
                ->add(
                    'kod_pocztowy', 'text', array(
                    'label' => 'kod pocztowy',
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Regex(
                            array(
                            'pattern' => '/^\d{2}-\d{3}$/',
                            'message' => 'Wprowadzane wartości muszą mieć'
                                . ' formę poprawnego kodu pocztowego'
                                )
                        )
                        )
                )
                )
                ->add(
                    'kontakt', 'text', array(
                    'required' => false,
                    'label' => 'numer kontaktowy',
                    'constraints' => array(
                        new Assert\Length(
                            array('max' => 11), new Assert\Regex(
                                array(
                        'pattern' => '/^\d{7,11}$/',
                    )
                            )
                        )
                        )
                )
                )
                ->add(
                    'klasa', 'choice', array(
                    'empty_value' => '',
                    'choices' => $klasyToSelect,
                    'constraints' => array(
                        new Assert\NotBlank())
                )
                )
                ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();


            $model = $this->_model->addUczen($data);


            return $app->redirect(
                $app['url_generator']->generate(
                    '/uczen/'
                ), 301
            );
            
        }
        return $app['twig']->render(
            'uczen/add.twig', array(
                'form' => $form->createView())
        );
    }
    
    
    /**
     * Edytuj dane ucznia
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function edit(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzUczen($id);

        if ($sprawdz) {
            $uczen = $this->_model->getUczen($id);

            //default values
            $data = array(
                'data' => $uczen['data_urodzenia'],
                'miejscowosc' => $uczen['miejscowosc'],
                'ulica' => $uczen['ulica'],
                'nr_domu' => $uczen['nr_domu'],
                'kod_pocztowy' => $uczen['kod_pocztowy'],
                'kontakt' => $uczen['kontakt'],
                'klasa' => $uczen['idKlasa'],
            );

            if (count($uczen)) {
                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'id', 'hidden', array(
                                'data' => $id,)
                        )
                        ->add(
                            'data', 'date', array(
                            'input' => 'string',
                            'widget' => 'choice',
                            'years' => range(1990, 2014),
                            'format' => 'yyyy-MM-dd',
                            'label' => 'data urodzenia',
                            'empty_value' => '',
                        )
                        )
                        ->add(
                            'miejscowosc', 'text', array(
                            'label' => 'miejscowość',
                            'constraints' => array(
                                new Assert\NotBlank())
                        )
                        )
                        ->add(
                            'ulica', 'text', array(
                            'constraints' => array(
                                new Assert\NotBlank())
                        )
                        )
                        ->add(
                            'nr_domu', 'text', array(
                            'constraints' => array(
                                new Assert\NotBlank())
                        )
                        )
                        ->add(
                            'kod_pocztowy', 'text', array(
                            'label' => 'kod pocztowy',
                            'constraints' => array(
                                new Assert\NotBlank())
                        )
                        )
                        ->add(
                            'kontakt', 'text', array(
                            'label' => 'numer kontaktowy',
                            'constraints' => array(
                                new Assert\Length(
                                    array('max' => 11)
                                )
                                )
                        )
                        )
                        ->add(
                            'klasa', 'text', array(
                            'constraints' => array(
                                new Assert\NotBlank())
                        )
                        )
                        ->getForm();
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $data = $form->getData();

                    try {
                        $model = $this->_model->editUczen($data);
                        $app['session']->getFlashBag()->add(
                            'message', array(
                                    'type' => 'success',
                                    'content' => 'Dane zostały zmienione')
                        );
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/uczen/'
                            ), 301
                        );
                    } catch (Exception $ex) {
                        $errors[] = 'Nieoczekiwany błąd';
                    }
                }
                return $app['twig']->render(
                    'uczen/edit.twig', array(
                        'form' => $form->createView())
                );
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                            'type' => 'danger',
                            'content' => 'Brak ucznia w systemie')
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/uczen/add'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                    'type' => 'danger',
                        'content' => 'Brak ucznia w systemie')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/uczen/'
                ), 301
            );
        }
    }
    
    /**
     * Usuń dane ucznia
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function delete(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzUczen($id);

        if ($sprawdz) {
            $uczen = $this->_model->getUczen($id);
            $data = array();

            if (count($uczen)) {
                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'iduser', 'hidden', array(
                                'data' => $id,)
                        )
                        ->add('Tak', 'submit')
                        ->add('Nie', 'submit')
                        ->getForm();
                $form->handleRequest($request);

                if ($form->isValid()) {

                    if ($form->get('Tak')->isClicked()) {
                        $data = $form->getData();
                        try {
                            $model = $this->_model->usunDaneUczen($data);
                            $app['session']->getFlashBag()->add(
                                'message', array(
                                        'type' => 'success',
                                        'content' => 'Dodatkowe dane usunięte')
                            );
                            return $app->redirect(
                                $app['url_generator']->generate(
                                    '/uczen/'
                                ), 301
                            );
                        } catch (Exception $ex) {
                            $errors[] = 'Nastąpił nieoczekiwany błąd';
                        }
                    } else {
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/uczen/'
                            ), 301
                        );
                    }
                } else {
                    return $app['twig']->render(
                        'uczen/delete.twig', array(
                                'form' => $form->createView())
                    );
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                            'type' => 'danger',
                            'content' => 'Brak ucznia w systemie')
                );
                $app['session']->getFlashBag()->set(
                    'error', 'Brak ucznia'
                );
                
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/uczen/'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                        'type' => 'danger',
                        'content' => 'Brak ucznia')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/uczen/'
                ), 301
            );
        }
    }


}
