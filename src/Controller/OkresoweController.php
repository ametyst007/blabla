<?php
/**
 * Okresowe Controller
 * 
 * PHP version 5
 * 
 * @category Controller
 * @package Controller
 * @author Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link wierzba.wzks.uj.edu.pl/~12_gorgolewska
 */
namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\OkresoweModel;
use Model\PrzedmiotModel;
use Model\UserModel;
use Model\UczenModel;

/**
 * Class OkresoweController
 * 
 * @category Controller
 * @package  Controller
 * @author Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Silex\Application
 * @uses Silex\ControllerProviderInterface
 * @uses Symfony\Component\HttpFoundation\Request
 * @uses Symfony\Component\Validator\Constraints
 * @uses Model\OkresoweModel
 * @uses Model\PrzedmiotModel
 * @uses Model\UserModel
 * @uses Model\UczenModel
 */
class OkresoweController implements ControllerProviderInterface
{
    /**
     * OkresoweModel object
     * 
     * @var $_model
     * @access protected
     */
    protected $_model;
    
    /**
     * Connection
     *
     * @param Application $app application object
     *
     * @access public
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->_model = new OkresoweModel($app);
        $this->_user = new UserModel($app);
        $ocenaController = $app['controllers_factory'];
        $ocenaController->get('/', array($this, 'index'))->bind('/okresowe/');
        $ocenaController->get('/student/', array($this, 'student'))
                ->bind('/okresowe/student');
        $ocenaController->match('/add/', array($this, 'add'))
                ->bind('/okresowe/add');
        $ocenaController->match('/edit/{id}', array($this, 'edit'))
                ->bind('/okresowe/edit');
        return $ocenaController;
    }
    
    /**
     * Wyświetl listę wszystkich wystawionych ocen okresowych 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function index(Application $app)
    {
        $ocenaModel = new OkresoweModel($app);
        $oceny = $ocenaModel->getOkresowe();
        return $app['twig']->render(
            'okresowe/index.twig', array(
                'oceny' => $oceny)
        );
    }
    /**
     * Wyświetl listę ocen okresowych zalogowanego ucznia 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function student(Application $app)
    {
        $id = $this->_user->getIdCurrentUser($app);

        $ocenaModel = new OkresoweModel($app);
        $oceny = $ocenaModel->getOkresoweuczen($id);
        return $app['twig']->render(
            'okresowe/index.twig', array(
                'oceny' => $oceny)
        );
    }
    
    /**
     * Dodaj nową ocenę okresową
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function add(Application $app, Request $request)
    {
        $przedmiotModel = new PrzedmiotModel($app);
        $przedmioty = $przedmiotModel->getAll();
        $przedmiotyToSelect = $przedmiotModel->przedmiotToSelect($przedmioty);

        $ucz = new UserModel($app);
        $szukanieucznia = $ucz->znajdzUczen();
        $uczniowieToSelect = $ucz->uczenToSelect($szukanieucznia);
        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add(
                    'idPrzedmiot', 'choice', array(
                    'empty_value' => '',
                    'label' => 'przedmiot',
                    'choices' => $przedmiotyToSelect,
                    'constraints' => array(new Assert\NotBlank())
                )
                )
                ->add(
                    'iduser', 'choice', array(
                    'label' => 'uczeń',
                    'empty_value' => '',
                    'choices' => $uczniowieToSelect,
                    'constraints' => array(
                        new Assert\NotBlank())
                )
                )
                ->add(
                    'osemestr', 'choice', array(
                    'label' => 'ocena semestralna',
                    'required' => false,
                    'empty_value' => '',
                    'choices' => array(1 => '1', 2 => '2', 3 => '3', 4 => '4',
                        5 => '5', 6 => '6'),
                    'constraints' => new Assert\Choice(array(1, 2, 3, 4, 5, 6)),
                )
                )
                ->add(
                    'orok', 'choice', array(
                    'label' => 'ocena roczna',
                    'required' => false,
                    'empty_value' => '',
                    'choices' => array(1 => '1', 2 => '2', 3 => '3', 4 => '4',
                        5 => '5', 6 => '6'),
                    'constraints' => new Assert\Choice(array(1, 2, 3, 4, 5, 6)),
                )
                )
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $check = $this->_model->getOcenaById(
                $data['iduser'], $data['idPrzedmiot']
            );

            if (!$check) {
                try {
                    $model = $this->_model->Okresowe($data);
                    $app['session']->getFlashBag()->add(
                        'message', array(
                            'type' => 'success',
                            'content' => 'Nowa ocena została dodana')
                    );
                    return $app->redirect(
                        $app['url_generator']->generate(
                            '/okresowe/'
                        ), 301
                    );
                } catch (Exception $ex) {
                    $error[] = 'Nieoczekiwany błąd';
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                    'type' => 'danger',
                    'content' => 'Ten uczeń ma już wystawioną'
                        . ' ocenę okresową z tego przedmiotu'
                )
                );
                return $app['twig']->render(
                    '/ocena/add.twig', array(
                            'form' => $form->createView()
                                )
                );
            }
        } else {

            return $app['twig']->render(
                '/ocena/add.twig', array(
                        'form' => $form->createView()
                            )
            );
        }

        return $app['twig']->render(
            '/ocena/add.twig', array(
                    'form' => $form->createView()
                        )
        );
    }
    
    /**
     * Edytuj wystawioną ocenę okresową
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function edit(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzokres($id);

        $przedmiotModel = new PrzedmiotModel($app);
        $przedmioty = $przedmiotModel->getAll();
        $przedmiotyToSelect = $przedmiotModel->przedmiotToSelect($przedmioty);

        $ucz = new UserModel($app);
        $szukanieucznia = $ucz->znajdzUczen();
        $uczniowieToSelect = $ucz->uczenToSelect($szukanieucznia);

        if ($sprawdz) {
            $ocena = $this->_model->getokres($id);

            //default values
            $data = array(
                'idPrzedmiot' => $ocena['idPrzedmiot'],
                'iduser' => $ocena['iduser'],
                'osemestr' => $ocena['ocena_semestr'],
                'orok' => $ocena['ocena_rok'],
            );



            if (count($ocena)) {

                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'id', 'hidden', array(
                            'data' => $id,
                        )
                        )
                        ->add(
                            'idPrzedmiot', 'choice', array(
                            'empty_value' => '',
                            'label' => 'przedmiot',
                            'choices' => $przedmiotyToSelect,
                            'constraints' => array(
                                new Assert\NotBlank())
                        )
                        )
                        ->add(
                            'iduser', 'choice', array(
                            'label' => 'uczeń',
                            'empty_value' => '',
                            'choices' => $uczniowieToSelect,
                            'constraints' => array(
                                new Assert\NotBlank())
                        )
                        )
                        ->add(
                            'osemestr', 'choice', array(
                            'label' => 'ocena semestralna',
                            'required' => false,
                            'empty_value' => '',
                            'choices' => array(1 => '1', 2 => '2', 3 => '3',
                                4 => '4', 5 => '5', 6 => '6'),
                            'constraints' => new Assert\Choice(
                                array(1, 2, 3, 4, 5, 6)
                            ),
                        )
                        )
                        ->add(
                            'orok', 'choice', array(
                            'label' => 'ocena roczna',
                            'required' => false,
                            'empty_value' => '',
                            'choices' => array(
                                1 => '1', 2 => '2', 3 => '3', 4 => '4',
                                5 => '5', 6 => '6'),
                            'constraints' => new Assert\Choice(
                                array(1, 2, 3, 4, 5, 6)
                            ),
                        )
                        )
                        ->getForm();

                $form->handleRequest($request);

                if ($form->isValid()) {

                    $data = $form->getData();

                    try {
                        $model = $this->_model->Okresoweedit($data);
                        $app['session']->getFlashBag()->add(
                            'message', array(
                                'type' => 'success',
                                'content' => 'Ocena została zmieniona')
                        );
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/okresowe/'
                            ), 301
                        );
                    } catch (Exception $ex) {
                        $error[] = 'Nieoczekiwany błąd';
                    }
                }
                return $app['twig']->render(
                    'ocena/edit.twig', array(
                        'form' => $form->createView())
                );
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                        'type' => 'danger',
                        'content' => 'Brak oceny w systemie')
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/okresowe/add'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                    'type' => 'danger', 'content' => 'Brak oceny')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/okresowe/'
                ), 301
            );
        }
    }

}
