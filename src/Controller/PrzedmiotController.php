<?php
/**
 * Przedmiot Controller
 * 
 * PHP version 5
 * 
 * @category Controller
 * @package  Controller
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 */
namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\PrzedmiotModel;

/**
 * Class PrzedmiotController
 * 
 * @category Controller
 * @package  Controller
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Silex\Application
 * @uses Silex\ControllerProviderInterface
 * @uses Symfony\Component\HttpFoundation\Request
 * @uses Symfony\Component\Validator\Constraints
 * @uses Model\PrzedmiotModel
 */
class PrzedmiotController implements ControllerProviderInterface
{
    /**
     * PrzedmiotModel object
     * 
     * @var $_model
     * @access protected
     */
    protected $_model;
    
    /**
     * Connection
     *
     * @param Application $app application object
     *
     * @access public
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->_model = new PrzedmiotModel($app);
        $przedmiotController = $app['controllers_factory'];
        $przedmiotController->get('/', array($this, 'index'))
                ->bind('/przedmiot/');
        $przedmiotController->match('/add/', array($this, 'add'))
                ->bind('/przedmiot/add');
        $przedmiotController->match('/edit/{id}', array($this, 'edit'))
                ->bind('/przedmiot/edit');
        $przedmiotController->match('/delete/{id}', array($this, 'delete'))
                ->bind('/przedmiot/delete');
        return $przedmiotController;
    }
    
    /**
     * Wyświetl listę przedmiotów 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function index(Application $app)
    {
        $przedmiotModel = new PrzedmiotModel($app);
        $przedmioty = $przedmiotModel->getAll();
        
        return $app['twig']->render(
            'przedmiot/index.twig', array(
                'przedmioty' => $przedmioty)
        );
    }
    
    /**
     * Dodaj nowy przedmiot
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function add(Application $app, Request $request)
    {
        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add('id', 'hidden')
                ->add(
                    'nazwa', 'text', array(
                    'invalid_message' => 'Nazwa przedmiotu musi składać się'
                        . ' z przynajmniej trzech znaków',
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Length(
                            array('min' => 3)
                        ),
                        new Assert\Regex(
                            array(
                            'pattern' => "/^([a-zżźćńółęąś]+\s?)+$/",
                            'message' => 'Niepoprawna nazwa (poprawna zawiera'
                            . ' wyłącznie małe litery alfabetu i spacje)')
                        )
            )
                        )
                )
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $check = $this->_model->getPrzedmiotbyNazwa($data['nazwa']);
            if (!$check) {

                try {
                    $przedmiotModel = new PrzedmiotModel($app);
                    $model = $this->_model->addPrzedmiot($data);

                    $app['session']->getFlashBag()->add(
                        'message', array(
                        'type' => 'success',
                        'content' => 'Przedmiot został dodany'
                            )
                    );
                    return $app->redirect(
                        $app['url_generator']->generate(
                            '/przedmiot/'
                        ), 301
                    );
                } catch (Exception $ex) {
                    $errors[] = 'Nieoczekiwany błąd';
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                    'type' => 'danger',
                    'content' => 'Przedmiot o podanej nazwie już istnieje'
                        )
                );

                return $app['twig']->render(
                    '/przedmiot/add.twig', array(
                            'form' => $form->createView()
                                )
                );
            }
        } else {

            return $app['twig']->render(
                '/przedmiot/add.twig', array(
                        'form' => $form->createView()
                            )
            );
        }

        return $app['twig']->render(
            'przedmiot/add.twig', array(
                    'form' => $form->createView()
                        )
        );
    }
    
    /**
     * Edytuj przedmiot
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function edit(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzPrzedmiot($id);
        if ($sprawdz) {

            $przedmiot = $this->_model->getPrzedmiot($id);

            // default values
            $data = array(
                'nazwa' => $przedmiot['nazwa']
            );

            if (count($przedmiot)) {
                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'id', 'hidden', array(
                                'data' => $id,)
                        )
                        ->add(
                            'nazwa', 'text', array(
                            'invalid_message' => 
                                'Nazwa przedmiotu musi składać się'
                                . ' z przynajmniej trzech znaków',
                            'constraints' => array(
                                new Assert\NotBlank(), new Assert\Length(
                                    array('min' => 3)
                                ),
                                new Assert\Regex(
                                    array(
                                        'pattern' => "/^([a-z]{2,}\s?)+$/",
                                        'message' => 'Niepoprawna nazwa'
                                        . ' (poprawna zawiera wyłącznie'
                                        . ' małe litery alfabetu'
                                        . ' i spacje)')
                                )
                    ))
                        )
                        ->getForm();
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $data = $form->getData();
                    try {
                        $model = $this->_model->editPrzedmiot($data);
                        $app['session']->getFlashBag()->add(
                            'message', array(
                                'type' => 'success',
                                'content' => 'Przedmiot zmieniony')
                        );
                        return $app->redirect(
                            $app['url_generator']->generate(
                                "/przedmiot/"
                            ), 301
                        );
                    } catch (Exception $ex) {
                        $errors[] = 'Nastąpił nieoczekiwany błąd';
                    }
                }

                return $app['twig']->render(
                    'przedmiot/edit.twig', array(
                        'form' => $form->createView())
                );
                
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                            'type' => 'danger',
                            'content' => 'Brak przedmiotu w systemie')
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/przedmiot/add'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                    'type' => 'danger', 'content' => 'Brak przedmiotu')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/przedmiot/'
                ), 301
            );
        }
    }
    
    /**
     * Usuń przedmiot
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function delete(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzPrzedmiot($id);

        if ($sprawdz) {
            $przedmiot = $this->_model->getPrzedmiot($id);
            $data = array();

            if (count($przedmiot)) {
                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'idPrzedmiot', 'hidden', array(
                                'data' => $id,)
                        )
                        ->add('Tak', 'submit')
                        ->add('Nie', 'submit')
                        ->getForm();
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if ($form->get('Tak')->isClicked()) {
                        $data = $form->getData();

                        try {
                            $model = $this->_model->usunPrzedmiot($data);
                            $app['session']->getFlashBag()->add(
                                'message', array(
                                    'type' => 'success',
                                    'content' => 'Przedmiot usunięty')
                            );
                            return $app->redirect(
                                $app['url_generator']->generate(
                                    '/przedmiot/'
                                ), 301
                            );
                        } catch (Exception $ex) {
                            $errors[] = 'Nastąpił nieoczekiwany błąd';
                        }
                    } else {
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/przedmiot/'
                            ), 301
                        );
                    }
                } else {
                    return $app['twig']->render(
                        'przedmiot/delete.twig', array(
                            'form' => $form->createView())
                    );
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                        'type' => 'danger',
                        'content' => 'Brak przedmiotu')
                );
                $app['session']->getFlashBag()->set(
                    'error', 'Brak przedmiotu w systemie'
                );
                return $app->redirect(
                    $app['utl_generator']->generate(
                        '/przedmiot/'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                        'type' => 'danger',
                        'content' => 'Brak przedmiotu')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/przedmiot/'
                ), 301
            );
        }
    }

   

}
