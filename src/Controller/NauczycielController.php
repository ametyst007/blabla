<?php
/**
 * Nauczyciel - Controller
 * 
 * PHP version 5
 * 
 * @category Controller
 * @package  Controller
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 */
namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\NauczycielModel;
use Model\KlasaModel;
use Model\PrzedmiotModel;

/**
 * Class NauczycielController
 * 
 * @category Controller
 * @package  Controller
 * @author Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Silex\Application
 * @uses Silex\ControllerProviderInterface
 * @uses Symfony\Component\HttpFoundation\Request
 * @uses Symfony\Component\Validator\Constraints
 * @uses Model\NauczycielModel
 * @uses Model\KlasaModel
 * @uses Model\PrzedmiotModel
 */
class NauczycielController implements ControllerProviderInterface
{
    /**
     * NauczycielModel object
     * 
     * @var $_model
     * @access protected
     */
    protected $_model;
    
    /**
     * Connection
     *
     * @param Application $app application object
     *
     * @access public
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->_model = new NauczycielModel($app);
        $nauczycielController = $app['controllers_factory'];
        $nauczycielController->get('/', array($this, 'index'))
                ->bind('/nauczyciel/');
        $nauczycielController->match('/add/', array($this, 'add'))
                ->bind('/nauczyciel/add');
        $nauczycielController->match('/edit/{id}', array($this, 'edit'))
                ->bind('/nauczyciel/edit');
        $nauczycielController->match('/delete/{id}', array($this, 'delete'))
                ->bind('/nauczyciel/delete');
        $nauczycielController->get('/view/{id}', array($this, 'view'))
                ->bind('/nauczyciel/view');
        return $nauczycielController;
    }
    
    /**
     * Wyświetl listę nauczycieli 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function index(Application $app)
    {
        $nauczycielModel = new NauczycielModel($app);
        $nauczyciele = $nauczycielModel->daneNauczyciel();
        return $app['twig']->render(
            'nauczyciel/index.twig', array(
                    'nauczyciele' => $nauczyciele)
        );
    }
    
    /**
     * Dodaj dane nowego nauczyciela
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function add(Application $app, Request $request)
    {

        $przedmiotModel = new PrzedmiotModel($app);
        $przedmioty = $przedmiotModel->getAll();
        $przedmiotyToSelect = $przedmiotModel->przedmiotToSelect($przedmioty);

        $klasaModel = new KlasaModel($app);
        $klasy = $klasaModel->getAll();
        $klasyToSelect = $klasaModel->klasaToSelect($klasy);
        $idarray = $this->_model->lastID();
        $id = (int) $idarray['iduser'];
        // var_dump($id);die();
        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add('id', 'hidden', array('data' => $id,))
                ->add(
                    'idKlasa', 'choice', array(
                    'required' => false,
                    'empty_value' => '',
                    'label' => 'wychowawca klasy',
                    'choices' => $klasyToSelect
                )
                )
                ->add(
                    'tytul', 'choice', array(
                    'label' => 'tytuł naukowy',
                    'empty_value' => '',
                    'choices' => array(
                        'lic.' => 'lic.', 'inż.' => 'inż.','mgr' => 'mgr',
                        'mgr inż.' => 'mgr inż.', 'dr' => 'dr',
                        'inne' => 'inne'),
                )
                )
                ->add(
                    'przedmiot', 'choice', array(
                    'label' => 'Przedmioty, których ma uczyć nauczyciel',
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $przedmiotyToSelect,
                )
                )
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $check = $this->_model->getwychowawca($data['idKlasa']);


            if (!$check) {

                try {
                    $nauczycielModel = new NauczycielModel($app);

                    $model = $this->_model->addNauczyciel($data);
                    $this->_model->przedmiotyNauczyciel($data);

                    $app['session']->getFlashBag()->add(
                        'message', array(
                        'type' => 'success',
                        'content' => 'Proces dodawania nauczyciela'
                            . ' został zakończony'
                            )
                    );
                    return $app->redirect(
                        $app['url_generator']->generate(
                            '/nauczyciel/'
                        ), 301
                    );
                    
                } catch (Exception $ex) {
                    $errors[] = 'Nieoczekiwany błąd';
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                    'type' => 'danger',
                    'content' => 'Ta klasa ma już wychowawcę'
                )
                );
                return $app['twig']->render(
                    '/nauczyciel/add.twig', array(
                            'form' => $form->createView()
                                )
                );
            }
        } else {

            return $app['twig']->render(
                '/nauczyciel/add.twig', array(
                        'form' => $form->createView()
                            )
            );
        }

        return $app['twig']->render(
            'nauczyciel/add.twig', array(
                    'form' => $form->createView()
                        )
        );
    }
    
    /**
     * Edytuj dane nauczyciela
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function edit(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzNauczyciel($id);

        if ($sprawdz) {
            $nauczyciel = $this->_model->getNauczyciel($id);

//default values
            $data = array(
                'idKlasa' => $nauczyciel['idKlasa'],
                'tytul' => $nauczyciel['tytul'],
            );
            $klasaModel = new klasaModel($app);
            $klasy = $klasaModel->getAll();

            $klasyToSelect = $klasaModel->klasaToSelect($klasy);
            if (count($nauczyciel)) {
                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'id', 'hidden', array(
                            'data' => $id,)
                        )
                        ->add(
                            'idKlasa', 'choice', array(
                            'empty_value' => '',
                            'label' => 'wychowawca klasy',
                            'choices' => $klasyToSelect
                        )
                        )
                        ->add(
                            'tytul', 'choice', array(
                            'label' => 'tytuł naukowy',
                            'choices' => array(
                                'lic.' => 'lic.', 'inż.' => 'inż.',
                                'mgr' => 'mgr', 'mgr inż.' => 'mgr inż.',
                                'dr' => 'dr', 'inne' => 'inne'),
                        )
                        )
                        ->getForm();

                $form->handleRequest($request);

                if ($form->isValid()) {
                    $data = $form->getData();

                    try {
                        $model = $this->_model->editNauczyciel($data);

                        $app['session']->getFlashBag()->add(
                            'message', array(
                                    'type' => 'success',
                                'content' => 'Dane zostały zmienione')
                        );
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/nauczyciel/'
                            ), 301
                        );
                    } catch (Exception $ex) {
                        $errors[] = 'Nastąpił nieoczekiwany błąd';
                    }
                }
                return $app['twig']->render(
                    'nauczyciel/edit.twig', array(
                            'form' => $form->createView())
                );
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array('type' => 'danger',
                            'content' => 'Brak nauczyciela w systemie')
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/nauczyciel/add'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array('type' => 'danger',
                        'content' => 'Brak nauczyciela')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/nauczyciel/'
                ), 301
            );
        }
    }
    
    /**
     * Usuń dodatkowe informacje o nauczycielu
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function delete(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzNauczyciel($id);

        if ($sprawdz) {
            $nauczyciel = $this->_model->getNauczyciel($id);
            $data = array();

            if (count($nauczyciel)) {
                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add('iduser', 'hidden', array('data' => $id,))
                        ->add('Tak', 'submit')
                        ->add('Nie', 'submit')
                        ->getForm();
                $form->handleRequest($request);

                if ($form->isValid()) {

                    if ($form->get('Tak')->isClicked()) {
                        $data = $form->getData();
                        try {
                            $model = $this->_model->usunDaneNauczyciel($data);
                            $app['session']->getFlashBag()->add(
                                'message', array(
                                        'type' => 'success',
                                    'content' => 'Dodatkowe dane usunięte')
                            );
                            return $app->redirect(
                                $app['url_generator']->generate(
                                    '/nauczyciel/'
                                ), 301
                            );
                        } catch (Exception $ex) {
                            $errors[] = 'Nastąpił nieoczekiwany błąd';
                        }
                    } else {
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/nauczyciel/'
                            ), 301
                        );
                    }
                } else {
                    return $app['twig']->render(
                        'nauczyciel/delete.twig', array(
                                'form' => $form->createView())
                    );
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                            'type' => 'danger',
                        'content' => 'Brak nauczyciela w systemie')
                );
                $app['session']->getFlashBag()->set(
                    'error', 'Brak nauczyciela'
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/nauczyciel/'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                        'type' => 'danger', 'content' => 'Brak nauczyciela')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/nauczyciel/'
                ), 301
            );
        }
    }
    /**
     * Podgląd informacji o jednym nauczycielu
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function view(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzNauczyciel($id);

        if ($sprawdz) {
            $nauczyciel = $this->_model->danejedenNauczyciel($id);

            if (count($nauczyciel)) {
                return $app['twig']->render(
                    'nauczyciel/view.twig', array(
                        'nauczyciel' => $nauczyciel
                                )
                );
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                    'type' => 'danger',
                    'content' => 'Nie znaleziono użytkownika'
                        )
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                        'type' => 'danger',
                    'content' => 'Brak nauczyciela')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/nauczyciel/'
                ), 301
            );
        }
    }

}
