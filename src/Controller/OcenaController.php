<?php
/**
 * Ocena Controller
 * 
 * PHP version 5
 * 
 * @category Controller
 * @package  Controller
 * @author Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link wierzba.wzks.uj.edu.pl/~12_gorgolewska
 */
namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\OcenaModel;
use Model\PrzedmiotModel;
use Model\UserModel;
use Model\UczenModel;
use Model\KlasaModel;

/**
 * Class OcenaController
 * 
 * @category Controller
 * @package  Controller
 * @author Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Silex\Application
 * @uses Silex\ControllerProviderInterface
 * @uses Symfony\Component\HttpFoundation\Request
 * @uses Symfony\Component\Validator\Constraints
 * @uses Model\OcenaModel
 * @uses Model\PrzedmiotModel
 * @uses Model\UserModel
 * @uses Model\UczenModel
 * @uses Model\KlasaModel
 */
class OcenaController implements ControllerProviderInterface
{
    /**
     * OcenaModel object
     * 
     * @var $_model
     * @access protected
     */
    protected $_model;
    
    /**
     * Connection
     *
     * @param Application $app application object
     *
     * @access public
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->_model = new OcenaModel($app);
        $this->_user = new UserModel($app);
        $this->_przedmiot = new PrzedmiotModel($app);
        $ocenaController = $app['controllers_factory'];
        $ocenaController->get('/opcje/', array($this, 'opcje'))
                ->bind('/ocena/opcje');
        $ocenaController->get('/', array($this, 'index'))->bind('/ocena/');
        $ocenaController->get('/student/', array($this, 'student'))
                ->bind('/ocena/student');
        $ocenaController->match('/przedmiot/', array($this, 'przedmiot'))
                ->bind('/ocena/przedmiot');
        $ocenaController->match('/uczen/', array($this, 'uczen'))
                ->bind('/ocena/uczen');
        $ocenaController->match('/indexwybrane/', array($this, 'indexwybrane'))
                ->bind('/ocena/indexwybrane');
        $ocenaController->match('/add/', array($this, 'add'))
                ->bind('/ocena/add');
        $ocenaController->match('/edit/{id}', array($this, 'edit'))
                ->bind('/ocena/edit');
        $ocenaController->match('/delete/{id}', array($this, 'delete'))
                ->bind('/ocena/delete');
        $ocenaController->get('/avg/', array($this, 'avg'))
                ->bind('/ocena/avg');
        $ocenaController->match('/avg_all', array($this, 'avg_all'))
                ->bind('/ocena/avg_all');
        return $ocenaController;
    }
    
    /**
     * Options
     *
     * @param Application $app application object
     *
     * @access public
     * @return mixed Generates page
     */
    public function opcje(Application $app)
    {

        return $app['twig']->render('ocena/opcje.twig');
    }
    
    /**
     * Wyświetl pełną listę ocen 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function index(Application $app)
    {
        $ocenaModel = new OcenaModel($app);
        $oceny = $ocenaModel->getAll();
        
        return $app['twig']->render(
            'ocena/index.twig', array(
                    'oceny' => $oceny)
        );
    }
    
    /**
     * Wyświetl listę ocen zalogowanego ucznia
     * 
     * @param Application $app      application object
     * 
     * @access public
     * @return mixed Generates page
     */
    public function student(Application $app)
    {

        $id = $this->_user->getIdCurrentUser($app);

        $gradeModel = new UczenModel($app);
        $grades = $gradeModel->ocenyUcznia($id);
        return $app['twig']->render(
            'ocena/indexuczen.twig', array(
                    'oceny' => $grades)
        );
    }
    
    /**
     * Wyświetl listę ocen według wybranego przedmiotu
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function przedmiot(Application $app, Request $request)
    {
        $przedmiotModel = new PrzedmiotModel($app);
        $przedmioty = $przedmiotModel->getAll();
        $przedmiotyToSelect = $przedmiotModel->przedmiotToSelect($przedmioty);

        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add(
                    'idPrzedmiot', 'choice', array(
                    'label' => 'przedmiot',
                    'empty_value' => '',
                    'choices' => $przedmiotyToSelect,
                )
                )
                ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $ocenaModel = new OcenaModel($app);
            $oceny = $ocenaModel->getWybranePrzedmiot($data);
            return $app['twig']->render(
                'ocena/index.twig', array(
                        'oceny' => $oceny)
            );
        }
        return $app['twig']->render(
            'ocena/_list.twig', array(
                    'oceny' => $oceny,
                    'form' => $form->createView())
        );
    }
    
    /**
     * Wyświetl pełną listę ocen danego ucznia 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function uczen(Application $app, Request $request)
    {
        $ucz = new UserModel($app);
        $szukanieucznia = $ucz->znajdzUczen();
        $uczniowieToSelect = $ucz->uczenToSelect($szukanieucznia);

        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add(
                    'iduczen', 'choice', array(
                    'label' => 'uczeń',
                    'empty_value' => '',
                    'choices' => $uczniowieToSelect,
                )
                )
                ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();


            $ocenaModel = new OcenaModel($app);
            $oceny = $ocenaModel->getWybraneUczen($data);
            return $app['twig']->render(
                'ocena/wybrane.twig', array(
                    'oceny' => $oceny)
            );
        }
        return $app['twig']->render(
            'ocena/_list.twig', array(
                    'oceny' => $oceny,
                    'form' => $form->createView())
        );
    }
    
    /**
     * Wyświetl listę ocen danego ucznia z wybranego przedmiotu
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function indexwybrane(Application $app, Request $request)
    {
        $ucz = new UserModel($app);
        $szukanieucznia = $ucz->znajdzUczen();
        $uczniowieToSelect = $ucz->uczenToSelect($szukanieucznia);

        $przedmiotModel = new PrzedmiotModel($app);
        $przedmioty = $przedmiotModel->getAll();
        $przedmiotyToSelect = $przedmiotModel->przedmiotToSelect($przedmioty);

        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add(
                    'iduczen', 'choice', array(
                    'label' => 'uczeń',
                    'empty_value' => '',
                    'choices' => $uczniowieToSelect,
                )
                )
                ->add(
                    'idPrzedmiot', 'choice', array(
                    'label' => 'przedmiot',
                    'empty_value' => '',
                    'choices' => $przedmiotyToSelect,
                )
                )
                ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();


            $ocenaModel = new OcenaModel($app);
            $oceny = $ocenaModel->getWybrane($data);
            return $app['twig']->render(
                'ocena/wybrane.twig', array(
                        'oceny' => $oceny)
            );
        }
        return $app['twig']->render(
            'ocena/_list.twig', array(
                    'oceny' => $oceny,
                    'form' => $form->createView())
        );
    }
    
    /**
     * Dodaj nową ocenę
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function add(Application $app, Request $request)
    {
        $aktualnadata = date('Y-m-d');
        $przedmiotModel = new PrzedmiotModel($app);
        $przedmioty = $przedmiotModel->getAll();
        $przedmiotyToSelect = $przedmiotModel->przedmiotToSelect($przedmioty);

        $ucz = new UserModel($app);
        $szukanieucznia = $ucz->znajdzUczen();
        $uczniowieToSelect = $ucz->uczenToSelect($szukanieucznia);
        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add(
                    'stopien', 'choice', array(
                    'empty_value' => '',
                    'choices' => array(1 => '1', 2 => '2', 3 => '3', 4 => '4',
                        5 => '5', 6 => '6'),
                    'constraints' => new Assert\Choice(
                        array(1, 2, 3, 4, 5, 6)
                    ),
                )
                )
                ->add(
                    'data', 'date', array(
                    'input' => 'string',
                    'widget' => 'single_text',
                    'constraints' => array(
                        new Assert\Date())      
                )
                )
                ->add(
                    'idPrzedmiot', 'choice', array(
                    'empty_value' => '',
                    'label' => 'przedmiot',
                    'choices' => $przedmiotyToSelect,
                    'constraints' => array(
                        new Assert\NotBlank())
                )
                )
                ->add(
                    'iduser', 'choice', array(
                    'label' => 'uczeń',
                    'empty_value' => '',
                    'choices' => $uczniowieToSelect,
                    'constraints' => array(
                        new Assert\NotBlank())
                )
                )
                ->add(
                    'komentarz', 'textarea'
                )
              
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            try {
                if ($data['data'] > $aktualnadata) {
                    $app['session']->getFlashBag()->add(
                        'message', array(
                                'type' => 'danger',
                                'content' => 'Nie można wystawić oceny'
                                . ' z datą naprzód')
                    );
                } else {

                    $check = $this->_przedmiot->klasaPrzedmiot($data);
                    if ($check) {
                      
                        if (!$app['security']->isGranted('ROLE_ADMIN')) {
                                $id = $this->_user->getIdCurrentUser($app);
                                
                                $sprawdz = $this->_przedmiot->
                                        nauczycielPrzedmiot($data, $id);
                                if (!$sprawdz) {
                                 $app['session']->getFlashBag()->add(
                                     'message', array(
                                        'type' => 'danger',
                            'content' => 'Nauczyciel nieupoważniony do
                                wystawienia oceny')
                                 );
                                
                                }     
                        }
                        if ($app['security']->isGranted('ROLE_ADMIN') ||
                                $sprawdz) {
                        $model = $this->_model->addOcena($data);


                        $app['session']->getFlashBag()->add(
                            'message', array(
                                'type' => 'success',
                                'content' => 'Nowa ocena została dodana')
                        );
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/ocena/'
                            ), 301
                        );
                        }
                    } else {
                        $app['session']->getFlashBag()->add(
                            'message', array(
                            'type' => 'danger',
                            'content' => 'Nie można wystawić oceny,'
                                .' uczeń nie jest zapisany na ten przedmiot')
                        );
                    }
                }
            } catch (Exception $ex) {
                $error[] = 'Nieoczekiwany błąd';
            }
        }
        return $app['twig']->render(
            'ocena/add.twig', array(
                'form' => $form->createView())
        );
    }
    
    /**
     * Edytuj ocenę
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function edit(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);

        $sprawdz = $this->_model->sprawdzOcena($id);

        if ($sprawdz) {
            $ocena = $this->_model->getOcena($id);

            $aktualnadata = date('Y-m-d');
            $przedmiotModel = new PrzedmiotModel($app);
            $przedmioty = $przedmiotModel->getAll();
            $przedmiotyToSelect = $przedmiotModel->
                    przedmiotToSelect($przedmioty);

            $ucz = new UserModel($app);
            $szukanieucznia = $ucz->znajdzUczen();
            $uczniowieToSelect = $ucz->uczenToSelect($szukanieucznia);
            //default values
            $data = array(
                'stopien' => $ocena['stopien'],
                'data' => $ocena['data_wystawienia'],
                'idPrzedmiot' => $ocena['idPrzedmiot'],
                'iduser' => $ocena['iduser'],
                'komentarz' => $ocena['komentarz'],
            );

            if (count($ocena)) {

                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'id', 'hidden', array(
                                'data' => $id,
                        )
                        )
                        ->add(
                            'stopien', 'choice', array(
                            'choices' => array(
                                1 => '1', 2 => '2', 3 => '3', 4 => '4',
                                5 => '5', 6 => '6'),
                            'constraints' => new Assert\Choice(
                                array(1, 2, 3, 4, 5, 6)
                            ),
                        )
                        )
                        ->add(
                            'data', 'date', array(
                            'input' => 'string',
                            'widget' => 'single_text',
                            'format' => 'yyyy-MM-dd',
                            'constraints' => array(new Assert\Date())
                        )
                        )
                        ->add(
                            'idPrzedmiot', 'choice', array(
                            'empty_value' => '',
                            'label' => 'przedmiot',
                            'choices' => $przedmiotyToSelect,
                            'constraints' => array(
                                new Assert\NotBlank())
                        )
                        )
                        ->add(
                            'iduser', 'choice', array(
                            'label' => 'uczeń',
                            'empty_value' => '',
                            'choices' => $uczniowieToSelect,
                            'constraints' => array(
                                new Assert\NotBlank())
                        )
                        )
                        ->add('komentarz', 'textarea')
                       
                        ->getForm();

                $form->handleRequest($request);

                if ($form->isValid()) {

                    $data = $form->getData();
                    try {
                        if ($data['data'] > $aktualnadata) {
                            $app['session']->getFlashBag()->add(
                                'message', array(
                                        'type' => 'danger',
                                        'content' => 'Nie można wystawić'
                                    . ' oceny z datą naprzód')
                            );
                        } else {

                            $check = $this->_przedmiot->klasaPrzedmiot($data);
                            if ($check) {
                                $model = $this->_model->saveOcena($data);
                                $app['session']->getFlashBag()->add(
                                    'message', array(
                                            'type' => 'success',
                                            'content' => 'Ocena zmieniona')
                                );
                                return $app->redirect(
                                    $app['url_generator']->generate(
                                        '/ocena/'
                                    ), 301
                                );
                            } else {
                                $app['session']->getFlashBag()->add(
                                    'message', array(
                                    'type' => 'danger',
                                    'content' => 'Nie można wystawić oceny,'
                                        . ' uczeń nie jest zapisany na'
                                        . ' ten przedmiot')
                                );
                            }
                        }
                    } catch (Exception $ex) {
                        $errors[] = 'Nastąpił nieoczekiwany błąd';
                    }
                }
                return $app['twig']->render(
                    'ocena/edit.twig', array(
                            'form' => $form->createView())
                );
                
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                        'type' => 'danger',
                        'content' => 'Brak oceny w systemie')
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/ocena/add'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                'type' => 'danger',
                'content' => 'Brak oceny')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/ocena/'
                ), 301
            );
        }
    }
    
    /**
     * Usuń ocenę
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function delete(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzOcena($id);

        if ($sprawdz) {
            $ocena = $this->_model->getOcena($id);
            $data = array();

            if (count($ocena)) {
                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'idOcena', 'hidden', array(
                                'data' => $id,)
                        )
                        ->add('Tak', 'submit')
                        ->add('Nie', 'submit')
                        ->getForm();
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if ($form->get('Tak')->isClicked()) {
                        $data = $form->getData();

                        try {
                            $model = $this->_model->usunOcene($data);
                            $app['session']->getFlashBag()->add(
                                'message', array(
                                        'type' => 'success',
                                        'content' => 'Ocena usunięta')
                            );
                            return $app->redirect(
                                $app['url_generator']->generate(
                                    '/ocena/'
                                ), 301
                            );
                        } catch (Exception $ex) {
                            $errors[] = 'Nastąpił nieoczekiwany błąd';
                        }
                    } else {
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/ocena/'
                            ), 301
                        );
                    }
                } else {
                    return $app['twig']->render(
                        'ocena/delete.twig', array(
                            'form' => $form->createView())
                    );
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                        'type' => 'danger',
                        'content' => 'Brak oceny w systemie')
                );
                $app['session']->getFlashBag()->set(
                    'error', 'Brak oceny'
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/ocena/'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                    'type' => 'danger',
                    'content' => 'Brak oceny')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/ocena/'
                ), 301
            );
            
        }
    }

   
    /**
     * Wyświetl średnią ocen cząstkowych wszystkich uczniów
     * z wybranego przedmiotu
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function avg_all(Application $app, Request $request)
    {

        $przedmiotModel = new PrzedmiotModel($app);
        $przedmioty = $przedmiotModel->getAll();
        $przedmiotyToSelect = $przedmiotModel->przedmiotToSelect($przedmioty);

        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add(
                    'idPrzedmiot', 'choice', array(
                    'label' => 'przedmiot',
                    'empty_value' => '',
                    'choices' => $przedmiotyToSelect,
                )
                )
                ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $avgModel = new OcenaModel($app);
            $avg = $avgModel->avgOcenaAll($data['idPrzedmiot']);
            return $app['twig']->render(
                'ocena/avg.twig', array(
                    'oceny' => $avg)
            );
        }
        return $app['twig']->render(
            'ocena/avgall.twig', array(
                    'oceny' => $oceny,
                    'form' => $form->createView())
        );
    }
    
    /**
     * Wyświetla średnie ocen cząstkowych zalogowanego ucznia
     * z poszczególnych przedmiotów
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function avg(Application $app, Request $request)
    {
        $id = $this->_user->getIdCurrentUser($app);
        $user = $this->_user->getUser($id);


        $avgModel = new OcenaModel($app);
        $avg = $avgModel->avgOcena($id);
        return $app['twig']->render(
            'ocena/avg.twig', array(
                'oceny' => $avg)
        );
    }

}
