<?php
/**
 * Klasa Controller
 * 
 * PHP version 5
 * 
 * @category Controller
 * @package  Controller
 * @author   Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 */
namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\KlasaModel;
use Model\PrzedmiotModel;

/**
 * Class KlasaController
 * 
 * @category Controller
 * @package  Controller
 * @author Agnieszka Gorgolewska <agnieszka.gorgolewska@uj.edu.pl>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     wierzba.wzks.uj.edu.pl/~12_gorgolewska
 * @uses Silex\Application
 * @uses Silex\ControllerProviderInterface
 * @uses Symfony\Component\HttpFoundation\Request
 * @uses Symfony\Component\Validator\Constraints
 * @uses Model\KlasaModel
 * @uses Model\PrzedmiotModel
 */
class KlasaController implements ControllerProviderInterface
{
    /**
     * KlasaModel object
     * 
     * @var $_model
     * @access protected
     */
    protected $_model;
    
    /**
     * Connection
     *
     * @param Application $app application object
     *
     * @access public
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->_model = new KlasaModel($app);
        $klasaController = $app['controllers_factory'];
        $klasaController->get('/', array($this, 'index'))
                ->bind('/klasa/');
        $klasaController->match('/add/', array($this, 'add'))
                ->bind('/klasa/add');
        $klasaController->match('/edit/{id}', array($this, 'edit'))
                ->bind('/klasa/edit');
        $klasaController->match('/delete/{id}/', array($this, 'delete'))
                ->bind('/klasa/delete');
        return $klasaController;
    }
    
    /**
     * Wyświetl listę klas 
     * 
     * @param Application $app      application object
     * @param Request     $request  request
     * 
     * @access public
     * @return mixed Generates page
     */
    public function index(Application $app)
    {
        $klasaModel = new KlasaModel($app);
        $klasy = $klasaModel->getAll();
        return $app['twig']->render(
            'klasa/index.twig', array(
                'klasy' => $klasy)
        );
    }
    
    /**
     * Dodaj nową klasę
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function add(Application $app, Request $request)
    {
        $przedmiotModel = new PrzedmiotModel($app);
        $przedmioty = $przedmiotModel->getAll();
        $przedmiotyToSelect = $przedmiotModel->przedmiotToSelect($przedmioty);
        $data = array();
        $form = $app['form.factory']->createBuilder('form', $data)
                ->add('id', 'hidden')
                ->add(
                    'rocznik', 'choice', array(
                    'required' => true,
                    'empty_value' => 'Wybierz odpowiedni rocznik',
                    'choices' => array(1 => '1', 2 => '2', 3 => '3',
                        4 => '4', 5 => '5', 6 => '6'),
                )
                )
                ->add(
                    'oddzial', 'text', array(
                    'label' => 'oddział',
                    'required' => true,
                    'constraints' => array(
                        new Assert\NotBlank(), new Assert\Regex(
                            array('pattern' => "/^[A-Z]$/",
                            'message' =>
                                'Oddział powinien być oznaczony'
                                . ' dużą literą alfabetu łacinśkiego')
                        )
                        )
                )
                )
                ->add(
                    'przedmiot', 'choice', array(
                    'label' => 'Przedmioty, których będzie uczyć się klasa',
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $przedmiotyToSelect,
                )
                )
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $check = $this->_model->getKlasabyData(
                $data['rocznik'], $data['oddzial']
            );

            if (!$check) {

                try {
                    $klasaModel = new KlasaModel($app);
                    
                    $model = $this->_model->addKlasa($data);
                    
                    $idcheck = $this->_model->getKlasabyData(
                        $data['rocznik'], $data['oddzial']
                    );
                    
                   $idp = (int)$idcheck['idKlasa'];

                    $this->_model->przedmiotyKlasa($data, $idp);

                    $app['session']->getFlashBag()->add(
                        'message', array(
                        'type' => 'success',
                        'content' => 'Klasa została stworzona'
                            )
                    );
                    return $app->redirect(
                        $app['url_generator']->generate(
                            '/klasa/'
                        ), 301
                    );
                } catch (Exception $ex) {
                    $errors[] = 'Nieoczekiwany błąd';
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                    'type' => 'danger',
                    'content' => 'Klasa o podanych oznaczeniach już istnieje'
                        )
                );

                return $app['twig']->render(
                    '/klasa/add.twig', array(
                            'form' => $form->createView()
                                )
                );
            }
        } else {

            return $app['twig']->render(
                '/klasa/add.twig', array(
                        'form' => $form->createView()
                            )
            );
        }

        return $app['twig']->render(
            'klasa/add.twig', array(
                    'form' => $form->createView()
                        )
        );
    }
    
    /**
     * Edytuj klasę
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function edit(Application $app, Request $request)
    {


        $id = (int) $request->get('id', 0);
        //$klasaModel = new KlasaModel($app);
        $sprawdz = $this->_model->sprawdzKlase($id);
        if ($sprawdz) {
            $klasa = $this->_model->getKlasa($id);

            //default values

            $data = array(
                'rocznik' => $klasa['rocznik'],
                'oddzial' => $klasa['oddzial'],
            );

            if (count($klasa)) {
                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'id', 'hidden', array(
                            'data' => $id,
                        )
                        )
                        ->add(
                            'rocznik', 'choice', array(
                            'required' => true,
                            'empty_value' => 'Wybierz odpowiedni rocznik',
                            'choices' => array(1 => '1', 2 => '2', 3 => '3',
                                4 => '4', 5 => '5', 6 => '6'),
                        )
                        )
                        ->add(
                            'oddzial', 'text', array(
                            'label' => 'oddział',
                            'required' => true,
                            'constraints' => array(
                                new Assert\NotBlank(), new Assert\Regex(
                                    array('pattern' => "/^[A-Z]$/",
                                    'message' =>
                                        'Oddział powinien być oznaczony'
                                        . ' wyłącznie dużą literą'
                                        . ' alfabetu łacinśkiego')
                                )
                                )
                        )
                        )
                        ->getForm();


                $form->handleRequest($request);

                if ($form->isValid()) {
                    $data = $form->getData();

                    try {
                        $model = $this->_model->editKlasa($data);
                        $app['session']->getFlashBag()->add(
                            'message', array(
                                'type' => 'success',
                                'content' => 'Dane klasy zmienione')
                        );
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/klasa/'
                            ), 301
                        );
                    
                        
                    } catch (Exception $ex) {
                        $errors[] = 'Niestety nastąpił błąd';
                    }
                }



                return $app['twig']->render(
                    'klasa/edit.twig', array(
                            'form' => $form->createView())
                );
            } else {

                $app['session']->getFlashBag()->add(
                    'message', array(
                        'type' => 'danger',
                        'content' => 'Brak klasy w systemie')
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/klasa/add'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                        'type' => 'danger',
                        'content' => 'Brak klasy w systemie')
            );
            return $app->redirect(
                $app['url_generator']->generate(
                    '/klasa/'
                ), 301
            );
        }
    }
    
    /**
     * Usuń klasę
     * 
     * @param \Silex\Application $app
     * @param \Symfony\Component\HttpFoundation\Request $request
     * 
     * @access public
     * @return mixed Generates page or redirect
     */
    public function delete(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);
        $sprawdz = $this->_model->sprawdzKlase($id);

        if ($sprawdz) {
            $klasa = $this->_model->getKlasa($id);
            $data = array();

            if (count($klasa)) {
                $form = $app['form.factory']->createBuilder('form', $data)
                        ->add(
                            'id', 'hidden', array(
                            'data' => $id,
                        )
                        )
                        ->add('Tak', 'submit')
                        ->add('Nie', 'submit')
                        ->getForm();
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if ($form->get('Tak')->isClicked()) {
                        $data = $form->getData();
                        try {
                            $model = $this->_model->usunKlase($data);
                            $app['session']->getFlashBag()->add(
                                'message', array(
                                    'type' => 'success',
                                'content' => 'Klasa usunięta z systemu')
                            );
                            return $app->redirect(
                                $app['url_generator']->generate(
                                    '/klasa/view'
                                ), 301
                            );
                        } catch (Exception $ex) {
                            $errors[] = 'Nastąpił nieoczekiwany błąd';
                        }
                    } else {
                        return $app->redirect(
                            $app['url_generator']->generate(
                                '/klasa/'
                            ), 301
                        );
                    }
                } else {
                    return $app['twig']->render(
                        'klasa/delete.twig', array(
                                'form' => $form->createView())
                    );
                }
            } else {
                $app['session']->getFlashBag()->add(
                    'message', array(
                            'type' => 'danger',
                            'content' => 'Brak klasy w systemie')
                );
                $app['session']->getFlashBag()->set(
                    'error', 'Brak klasy'
                );
                return $app->redirect(
                    $app['url_generator']->generate(
                        '/klasa/'
                    ), 301
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'message', array(
                    'type' => 'danger', 'content' => 'Brak klasy')
            );
            
            return $app->redirect(
                $app['url_generator']->generate(
                    '/klasa/'
                ), 301
            );
        }
    }

 

}
