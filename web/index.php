<?php
/**
 * Główny plik projektu.
 * Definiuje kontrolery, usługi i uruchamia aplikację
 */
require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;
$app->register(new Silex\Provider\ValidatorServiceProvider());

/* Form */
$app->register(new Silex\Provider\FormServiceProvider());

/* Twig */
$app->register(
    new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../src/views',
)
);


/* Doctrine */
$app->register(
    new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'host' => '127.0.0.1',
        'dbname' => 'projekt_php',
        'user' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
    ),
)
);

$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(
    new Silex\Provider\TranslationServiceProvider(), array(
    'translator.domains' => array(),
)
);

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());


$app->register(
    new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            'pattern' => '^.*$',
            'form' => array(
                'login_path' => '/auth/login',
                'check_path' => '/auth/login_check',
                'default_target_path' => '/ok/',
                'username_parameter' => 'form[login]',
                'password_parameter' => 'form[haslo]',
            ),
            'logout' => true,
            'anonymous' => true,
            'logout' => array('logout_path' => '/auth/logout'),
            'users' => $app->share(
                function() use ($app) {
                        return new User\UserProvider($app);
                }
            ),
        ),
    ),
    'security.access_rules' => array(
        array(
            '^/auth/.+$|^/klasa/$|^/przedmiot/$',
            'IS_AUTHENTICATED_ANONYMOUSLY'),
        array(
            '^/ocena/student.*$|^/ocena/avg.*$|^/okresowe/student.*$|'
            . '^/user/view.*$|^/ok/$', 'ROLE_UCZEN'),
        array(
            '^/ocena/.*$', 'ROLE_NAUCZYCIEL'),
        array(
            '^/.+$', 'ROLE_ADMIN')
        //array('^/.+$', 'IS_AUTHENTICATED_ANONYMOUSLY') //chwilowe ustawienia
    ),
    'security.role_hierarchy' => array(
        'ROLE_ADMIN' => array(
            'ROLE_WYCHOWAWCA', 'ROLE_NAUCZYCIEL', 'ROLE_UCZEN',
            'ROLE_ANONYMUS'),
        'ROLE_WYCHOWAWCA' => array(
            'ROLE_NAUCZYCIEL', 'ROLE_UCZEN'),
        'ROLE_NAUCZYCIEL' => array(
            'ROLE_UCZEN')
    ),
)
);

$app->mount('/auth/', new Controller\AuthController());
$app->mount('/ocena/', new Controller\OcenaController());
$app->mount('/klasa/', new Controller\KlasaController());
$app->mount('/przedmiot/', new Controller\PrzedmiotController());
$app->mount('/user/', new Controller\UserController());
$app->mount('/uczen/', new Controller\UczenController());
$app->mount('/nauczyciel/', new Controller\NauczycielController());
$app->mount('/okresowe/', new Controller\OkresoweController());

$app->get(
    '/', function() use($app) {
    return $app['twig']->render('index.twig');
    }
);
$app->get(
    '/ok/', function() use($app) {
    return $app['twig']->render('logowanie.twig');
    }
);





$app->run();
