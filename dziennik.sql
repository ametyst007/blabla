CREATE DATABASE  IF NOT EXISTS `projekt_php` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `projekt_php`;
-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (i686)
--
-- Host: 127.0.0.1    Database: projekt_php
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `php_Klasa`
--

DROP TABLE IF EXISTS `php_Klasa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `php_Klasa` (
  `idKlasa` int(11) NOT NULL AUTO_INCREMENT,
  `rocznik` int(2) NOT NULL,
  `oddzial` varchar(45) NOT NULL,
  PRIMARY KEY (`idKlasa`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `php_Klasa`
--

LOCK TABLES `php_Klasa` WRITE;
/*!40000 ALTER TABLE `php_Klasa` DISABLE KEYS */;
/*!40000 ALTER TABLE `php_Klasa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `php_Nauczyciel`
--

DROP TABLE IF EXISTS `php_Nauczyciel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `php_Nauczyciel` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `tytul_naukowy` varchar(45) NOT NULL,
  `idKlasa` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `php_Nauczyciel`
--

LOCK TABLES `php_Nauczyciel` WRITE;
/*!40000 ALTER TABLE `php_Nauczyciel` DISABLE KEYS */;
/*!40000 ALTER TABLE `php_Nauczyciel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `php_Nauczyciel_Przedmiot`
--

DROP TABLE IF EXISTS `php_Nauczyciel_Przedmiot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `php_Nauczyciel_Przedmiot` (
  `id` int(11) NOT NULL,
  `idPrzedmiot` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `php_Nauczyciel_Przedmiot`
--

LOCK TABLES `php_Nauczyciel_Przedmiot` WRITE;
/*!40000 ALTER TABLE `php_Nauczyciel_Przedmiot` DISABLE KEYS */;
/*!40000 ALTER TABLE `php_Nauczyciel_Przedmiot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `php_Ocena`
--

DROP TABLE IF EXISTS `php_Ocena`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `php_Ocena` (
  `idOcena` int(11) NOT NULL AUTO_INCREMENT,
  `stopien` int(11) NOT NULL,
  `data_wystawienia` date NOT NULL,
  `idPrzedmiot` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `komentarz` text,
  PRIMARY KEY (`idOcena`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `php_Ocena`
--

LOCK TABLES `php_Ocena` WRITE;
/*!40000 ALTER TABLE `php_Ocena` DISABLE KEYS */;
INSERT INTO `php_Ocena` VALUES (1,1,'0000-00-00',11,11,'sdheshrys');
/*!40000 ALTER TABLE `php_Ocena` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `php_Przedmiot`
--

DROP TABLE IF EXISTS `php_Przedmiot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `php_Przedmiot` (
  `idPrzedmiot` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(45) NOT NULL,
  PRIMARY KEY (`idPrzedmiot`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `php_Przedmiot`
--

LOCK TABLES `php_Przedmiot` WRITE;
/*!40000 ALTER TABLE `php_Przedmiot` DISABLE KEYS */;
/*!40000 ALTER TABLE `php_Przedmiot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `php_Uczen`
--

DROP TABLE IF EXISTS `php_Uczen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `php_Uczen` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `data_urodzenia` date NOT NULL,
  `miejscowosc` char(50) NOT NULL,
  `ulica` char(30) NOT NULL,
  `nr_domu` varchar(45) NOT NULL,
  `kod_pocztowy` char(7) NOT NULL,
  `kontakt` char(10) DEFAULT NULL,
  `idKlasa` int(11) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `php_Uczen`
--

LOCK TABLES `php_Uczen` WRITE;
/*!40000 ALTER TABLE `php_Uczen` DISABLE KEYS */;
/*!40000 ALTER TABLE `php_Uczen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `php_Uczen_Przedmiot`
--

DROP TABLE IF EXISTS `php_Uczen_Przedmiot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `php_Uczen_Przedmiot` (
  `id` int(11) NOT NULL,
  `idPrzedmiot` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `php_Uczen_Przedmiot`
--

LOCK TABLES `php_Uczen_Przedmiot` WRITE;
/*!40000 ALTER TABLE `php_Uczen_Przedmiot` DISABLE KEYS */;
/*!40000 ALTER TABLE `php_Uczen_Przedmiot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `php_users`
--

DROP TABLE IF EXISTS `php_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `php_users` (
  `iduser` int(4) NOT NULL AUTO_INCREMENT,
  `login` char(45) NOT NULL,
  `haslo` char(45) NOT NULL,
  `imie` varchar(45) NOT NULL,
  `nazwisko` varchar(45) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `php_users`
--

LOCK TABLES `php_users` WRITE;
/*!40000 ALTER TABLE `php_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `php_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_WYCHOWAWCA'),(3,'ROLE_NAUCZYCIEL'),(4,'ROLE_UCZEN');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idroles` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-27 16:23:52
